
package com.intracom.ws.eopyy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for executeElectronicPrescriptionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="executeElectronicPrescriptionResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultBeanEPrescription" type="{http://eopyy.ws.intracom.com/}resultBeanEPrescription" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "executeElectronicPrescriptionResponse", propOrder = {
    "resultBeanEPrescription"
})
public class ExecuteElectronicPrescriptionResponse {

    @XmlElement(name = "ResultBeanEPrescription")
    protected ResultBeanEPrescription resultBeanEPrescription;

    /**
     * Gets the value of the resultBeanEPrescription property.
     * 
     * @return
     *     possible object is
     *     {@link ResultBeanEPrescription }
     *     
     */
    public ResultBeanEPrescription getResultBeanEPrescription() {
        return resultBeanEPrescription;
    }

    /**
     * Sets the value of the resultBeanEPrescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultBeanEPrescription }
     *     
     */
    public void setResultBeanEPrescription(ResultBeanEPrescription value) {
        this.resultBeanEPrescription = value;
    }

}

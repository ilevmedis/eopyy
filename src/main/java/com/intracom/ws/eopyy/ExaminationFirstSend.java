
package com.intracom.ws.eopyy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for examinationFirstSend complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="examinationFirstSend">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sequenceNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="usedFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eDapiCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "examinationFirstSend", propOrder = {
    "sequenceNumber",
    "usedFlag",
    "eDapiCode"
})
public class ExaminationFirstSend {

    protected int sequenceNumber;
    protected String usedFlag;
    protected String eDapiCode;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     */
    public int getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     */
    public void setSequenceNumber(int value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the usedFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsedFlag() {
        return usedFlag;
    }

    /**
     * Sets the value of the usedFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsedFlag(String value) {
        this.usedFlag = value;
    }

    /**
     * Gets the value of the eDapiCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEDapiCode() {
        return eDapiCode;
    }

    /**
     * Sets the value of the eDapiCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEDapiCode(String value) {
        this.eDapiCode = value;
    }

}

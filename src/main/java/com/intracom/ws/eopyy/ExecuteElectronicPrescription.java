
package com.intracom.ws.eopyy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for executeElectronicPrescription complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="executeElectronicPrescription">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EprescriptionEntry" type="{http://eopyy.ws.intracom.com/}eprescriptionEntry" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "executeElectronicPrescription", propOrder = {
    "eprescriptionEntry"
})
public class ExecuteElectronicPrescription {

    @XmlElement(name = "EprescriptionEntry")
    protected EprescriptionEntry eprescriptionEntry;

    /**
     * Gets the value of the eprescriptionEntry property.
     * 
     * @return
     *     possible object is
     *     {@link EprescriptionEntry }
     *     
     */
    public EprescriptionEntry getEprescriptionEntry() {
        return eprescriptionEntry;
    }

    /**
     * Sets the value of the eprescriptionEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link EprescriptionEntry }
     *     
     */
    public void setEprescriptionEntry(EprescriptionEntry value) {
        this.eprescriptionEntry = value;
    }

}

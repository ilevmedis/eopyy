
package com.intracom.ws.eopyy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for submissionCancelOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="submissionCancelOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="errNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="errtxt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "submissionCancelOut", propOrder = {
    "errNo",
    "errtxt"
})
public class SubmissionCancelOut {

    protected int errNo;
    protected String errtxt;

    /**
     * Gets the value of the errNo property.
     * 
     */
    public int getErrNo() {
        return errNo;
    }

    /**
     * Sets the value of the errNo property.
     * 
     */
    public void setErrNo(int value) {
        this.errNo = value;
    }

    /**
     * Gets the value of the errtxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrtxt() {
        return errtxt;
    }

    /**
     * Sets the value of the errtxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrtxt(String value) {
        this.errtxt = value;
    }

}

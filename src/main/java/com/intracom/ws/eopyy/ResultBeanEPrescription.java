
package com.intracom.ws.eopyy;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for resultBeanEPrescription complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="resultBeanEPrescription">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="kepaDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="kepaNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="kepaPercentage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prescriptNum" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="prescriptionDetails" type="{http://eopyy.ws.intracom.com/}prescriptionDetails" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prescriptionExceptions" type="{http://eopyy.ws.intracom.com/}ePrescriptionException" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="seqNUm" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="submissionOut" type="{http://eopyy.ws.intracom.com/}submissionOut" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resultBeanEPrescription", propOrder = {
    "kepaDate",
    "kepaNumber",
    "kepaPercentage",
    "prescriptNum",
    "prescriptionDetails",
    "prescriptionExceptions",
    "seqNUm",
    "submissionOut"
})
public class ResultBeanEPrescription {

    protected String kepaDate;
    protected String kepaNumber;
    protected String kepaPercentage;
    protected int prescriptNum;
    @XmlElement(nillable = true)
    protected List<PrescriptionDetails> prescriptionDetails;
    @XmlElement(nillable = true)
    protected List<EPrescriptionException> prescriptionExceptions;
    protected int seqNUm;
    protected SubmissionOut submissionOut;

    /**
     * Gets the value of the kepaDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKepaDate() {
        return kepaDate;
    }

    /**
     * Sets the value of the kepaDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKepaDate(String value) {
        this.kepaDate = value;
    }

    /**
     * Gets the value of the kepaNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKepaNumber() {
        return kepaNumber;
    }

    /**
     * Sets the value of the kepaNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKepaNumber(String value) {
        this.kepaNumber = value;
    }

    /**
     * Gets the value of the kepaPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKepaPercentage() {
        return kepaPercentage;
    }

    /**
     * Sets the value of the kepaPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKepaPercentage(String value) {
        this.kepaPercentage = value;
    }

    /**
     * Gets the value of the prescriptNum property.
     * 
     */
    public int getPrescriptNum() {
        return prescriptNum;
    }

    /**
     * Sets the value of the prescriptNum property.
     * 
     */
    public void setPrescriptNum(int value) {
        this.prescriptNum = value;
    }

    /**
     * Gets the value of the prescriptionDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prescriptionDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrescriptionDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrescriptionDetails }
     * 
     * 
     */
    public List<PrescriptionDetails> getPrescriptionDetails() {
        if (prescriptionDetails == null) {
            prescriptionDetails = new ArrayList<PrescriptionDetails>();
        }
        return this.prescriptionDetails;
    }

    /**
     * Gets the value of the prescriptionExceptions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prescriptionExceptions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrescriptionExceptions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EPrescriptionException }
     * 
     * 
     */
    public List<EPrescriptionException> getPrescriptionExceptions() {
        if (prescriptionExceptions == null) {
            prescriptionExceptions = new ArrayList<EPrescriptionException>();
        }
        return this.prescriptionExceptions;
    }

    /**
     * Gets the value of the seqNUm property.
     * 
     */
    public int getSeqNUm() {
        return seqNUm;
    }

    /**
     * Sets the value of the seqNUm property.
     * 
     */
    public void setSeqNUm(int value) {
        this.seqNUm = value;
    }

    /**
     * Gets the value of the submissionOut property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionOut }
     *     
     */
    public SubmissionOut getSubmissionOut() {
        return submissionOut;
    }

    /**
     * Sets the value of the submissionOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionOut }
     *     
     */
    public void setSubmissionOut(SubmissionOut value) {
        this.submissionOut = value;
    }

}

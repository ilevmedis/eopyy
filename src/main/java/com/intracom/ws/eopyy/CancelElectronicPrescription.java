
package com.intracom.ws.eopyy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cancelElectronicPrescription complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cancelElectronicPrescription">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EPrescriptionCancel" type="{http://eopyy.ws.intracom.com/}ePrescriptionCancel" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cancelElectronicPrescription", propOrder = {
    "ePrescriptionCancel"
})
public class CancelElectronicPrescription {

    @XmlElement(name = "EPrescriptionCancel")
    protected EPrescriptionCancel ePrescriptionCancel;

    /**
     * Gets the value of the ePrescriptionCancel property.
     * 
     * @return
     *     possible object is
     *     {@link EPrescriptionCancel }
     *     
     */
    public EPrescriptionCancel getEPrescriptionCancel() {
        return ePrescriptionCancel;
    }

    /**
     * Sets the value of the ePrescriptionCancel property.
     * 
     * @param value
     *     allowed object is
     *     {@link EPrescriptionCancel }
     *     
     */
    public void setEPrescriptionCancel(EPrescriptionCancel value) {
        this.ePrescriptionCancel = value;
    }

}

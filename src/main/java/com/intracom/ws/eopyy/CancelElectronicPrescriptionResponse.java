
package com.intracom.ws.eopyy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cancelElectronicPrescriptionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cancelElectronicPrescriptionResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubmissionOut" type="{http://eopyy.ws.intracom.com/}submissionCancelOut" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cancelElectronicPrescriptionResponse", propOrder = {
    "submissionOut"
})
public class CancelElectronicPrescriptionResponse {

    @XmlElement(name = "SubmissionOut")
    protected SubmissionCancelOut submissionOut;

    /**
     * Gets the value of the submissionOut property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionCancelOut }
     *     
     */
    public SubmissionCancelOut getSubmissionOut() {
        return submissionOut;
    }

    /**
     * Sets the value of the submissionOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionCancelOut }
     *     
     */
    public void setSubmissionOut(SubmissionCancelOut value) {
        this.submissionOut = value;
    }

}

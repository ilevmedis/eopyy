
package com.intracom.ws.eopyy;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ePrescription complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ePrescription">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="examinationFirstList" type="{http://eopyy.ws.intracom.com/}examinationFirst" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ama" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="amkaEUorg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EKAAExpiryDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ekaa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ekasBeneficiary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="error" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="errorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="euOrgid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="europeanFlg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="examinedAmka" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="examinedFirstname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="examinedLastname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="icd10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="icd10Descr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="institutionCodeDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredCountryDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="intangible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="intangibleFlg" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="issueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noPrivateCompensation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="onlyPublicExecution" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orgType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PPAStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ppa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prescrDocCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prescrDocFirstname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prescrDocLastname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prescriptComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="zeroPartId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ePrescription", propOrder = {
    "examinationFirstList",
    "ama",
    "amkaEUorg",
    "ekaaExpiryDate",
    "ekaa",
    "ekasBeneficiary",
    "error",
    "errorCode",
    "euOrgid",
    "europeanFlg",
    "examinedAmka",
    "examinedFirstname",
    "examinedLastname",
    "icd10",
    "icd10Descr",
    "institutionCodeDescr",
    "insuredCountryCode",
    "insuredCountryDescr",
    "intangible",
    "intangibleFlg",
    "issueDate",
    "noPrivateCompensation",
    "number",
    "onlyPublicExecution",
    "orgType",
    "ppaStartDate",
    "ppa",
    "prescrDocCode",
    "prescrDocFirstname",
    "prescrDocLastname",
    "prescriptComment",
    "status",
    "zeroPartId"
})
public class EPrescription {

    protected List<ExaminationFirst> examinationFirstList;
    protected String ama;
    protected String amkaEUorg;
    @XmlElement(name = "EKAAExpiryDate")
    protected String ekaaExpiryDate;
    protected String ekaa;
    protected String ekasBeneficiary;
    protected String error;
    protected String errorCode;
    protected String euOrgid;
    protected String europeanFlg;
    protected String examinedAmka;
    protected String examinedFirstname;
    protected String examinedLastname;
    protected String icd10;
    protected String icd10Descr;
    protected String institutionCodeDescr;
    protected String insuredCountryCode;
    protected String insuredCountryDescr;
    protected String intangible;
    protected Boolean intangibleFlg;
    protected String issueDate;
    protected String noPrivateCompensation;
    protected String number;
    protected String onlyPublicExecution;
    protected String orgType;
    @XmlElement(name = "PPAStartDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ppaStartDate;
    protected String ppa;
    protected String prescrDocCode;
    protected String prescrDocFirstname;
    protected String prescrDocLastname;
    protected String prescriptComment;
    protected String status;
    protected String zeroPartId;

    /**
     * Gets the value of the examinationFirstList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the examinationFirstList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExaminationFirstList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExaminationFirst }
     * 
     * 
     */
    public List<ExaminationFirst> getExaminationFirstList() {
        if (examinationFirstList == null) {
            examinationFirstList = new ArrayList<ExaminationFirst>();
        }
        return this.examinationFirstList;
    }

    /**
     * Gets the value of the ama property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAma() {
        return ama;
    }

    /**
     * Sets the value of the ama property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAma(String value) {
        this.ama = value;
    }

    /**
     * Gets the value of the amkaEUorg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmkaEUorg() {
        return amkaEUorg;
    }

    /**
     * Sets the value of the amkaEUorg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmkaEUorg(String value) {
        this.amkaEUorg = value;
    }

    /**
     * Gets the value of the ekaaExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEKAAExpiryDate() {
        return ekaaExpiryDate;
    }

    /**
     * Sets the value of the ekaaExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEKAAExpiryDate(String value) {
        this.ekaaExpiryDate = value;
    }

    /**
     * Gets the value of the ekaa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEkaa() {
        return ekaa;
    }

    /**
     * Sets the value of the ekaa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEkaa(String value) {
        this.ekaa = value;
    }

    /**
     * Gets the value of the ekasBeneficiary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEkasBeneficiary() {
        return ekasBeneficiary;
    }

    /**
     * Sets the value of the ekasBeneficiary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEkasBeneficiary(String value) {
        this.ekasBeneficiary = value;
    }

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setError(String value) {
        this.error = value;
    }

    /**
     * Gets the value of the errorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the value of the errorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

    /**
     * Gets the value of the euOrgid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEuOrgid() {
        return euOrgid;
    }

    /**
     * Sets the value of the euOrgid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEuOrgid(String value) {
        this.euOrgid = value;
    }

    /**
     * Gets the value of the europeanFlg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEuropeanFlg() {
        return europeanFlg;
    }

    /**
     * Sets the value of the europeanFlg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEuropeanFlg(String value) {
        this.europeanFlg = value;
    }

    /**
     * Gets the value of the examinedAmka property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExaminedAmka() {
        return examinedAmka;
    }

    /**
     * Sets the value of the examinedAmka property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExaminedAmka(String value) {
        this.examinedAmka = value;
    }

    /**
     * Gets the value of the examinedFirstname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExaminedFirstname() {
        return examinedFirstname;
    }

    /**
     * Sets the value of the examinedFirstname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExaminedFirstname(String value) {
        this.examinedFirstname = value;
    }

    /**
     * Gets the value of the examinedLastname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExaminedLastname() {
        return examinedLastname;
    }

    /**
     * Sets the value of the examinedLastname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExaminedLastname(String value) {
        this.examinedLastname = value;
    }

    /**
     * Gets the value of the icd10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcd10() {
        return icd10;
    }

    /**
     * Sets the value of the icd10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcd10(String value) {
        this.icd10 = value;
    }

    /**
     * Gets the value of the icd10Descr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcd10Descr() {
        return icd10Descr;
    }

    /**
     * Sets the value of the icd10Descr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcd10Descr(String value) {
        this.icd10Descr = value;
    }

    /**
     * Gets the value of the institutionCodeDescr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstitutionCodeDescr() {
        return institutionCodeDescr;
    }

    /**
     * Sets the value of the institutionCodeDescr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstitutionCodeDescr(String value) {
        this.institutionCodeDescr = value;
    }

    /**
     * Gets the value of the insuredCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredCountryCode() {
        return insuredCountryCode;
    }

    /**
     * Sets the value of the insuredCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredCountryCode(String value) {
        this.insuredCountryCode = value;
    }

    /**
     * Gets the value of the insuredCountryDescr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredCountryDescr() {
        return insuredCountryDescr;
    }

    /**
     * Sets the value of the insuredCountryDescr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredCountryDescr(String value) {
        this.insuredCountryDescr = value;
    }

    /**
     * Gets the value of the intangible property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntangible() {
        return intangible;
    }

    /**
     * Sets the value of the intangible property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntangible(String value) {
        this.intangible = value;
    }

    /**
     * Gets the value of the intangibleFlg property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIntangibleFlg() {
        return intangibleFlg;
    }

    /**
     * Sets the value of the intangibleFlg property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIntangibleFlg(Boolean value) {
        this.intangibleFlg = value;
    }

    /**
     * Gets the value of the issueDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssueDate() {
        return issueDate;
    }

    /**
     * Sets the value of the issueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssueDate(String value) {
        this.issueDate = value;
    }

    /**
     * Gets the value of the noPrivateCompensation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoPrivateCompensation() {
        return noPrivateCompensation;
    }

    /**
     * Sets the value of the noPrivateCompensation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoPrivateCompensation(String value) {
        this.noPrivateCompensation = value;
    }

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

    /**
     * Gets the value of the onlyPublicExecution property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnlyPublicExecution() {
        return onlyPublicExecution;
    }

    /**
     * Sets the value of the onlyPublicExecution property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnlyPublicExecution(String value) {
        this.onlyPublicExecution = value;
    }

    /**
     * Gets the value of the orgType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgType() {
        return orgType;
    }

    /**
     * Sets the value of the orgType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgType(String value) {
        this.orgType = value;
    }

    /**
     * Gets the value of the ppaStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPPAStartDate() {
        return ppaStartDate;
    }

    /**
     * Sets the value of the ppaStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPPAStartDate(XMLGregorianCalendar value) {
        this.ppaStartDate = value;
    }

    /**
     * Gets the value of the ppa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPpa() {
        return ppa;
    }

    /**
     * Sets the value of the ppa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPpa(String value) {
        this.ppa = value;
    }

    /**
     * Gets the value of the prescrDocCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrescrDocCode() {
        return prescrDocCode;
    }

    /**
     * Sets the value of the prescrDocCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrescrDocCode(String value) {
        this.prescrDocCode = value;
    }

    /**
     * Gets the value of the prescrDocFirstname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrescrDocFirstname() {
        return prescrDocFirstname;
    }

    /**
     * Sets the value of the prescrDocFirstname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrescrDocFirstname(String value) {
        this.prescrDocFirstname = value;
    }

    /**
     * Gets the value of the prescrDocLastname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrescrDocLastname() {
        return prescrDocLastname;
    }

    /**
     * Sets the value of the prescrDocLastname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrescrDocLastname(String value) {
        this.prescrDocLastname = value;
    }

    /**
     * Gets the value of the prescriptComment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrescriptComment() {
        return prescriptComment;
    }

    /**
     * Sets the value of the prescriptComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrescriptComment(String value) {
        this.prescriptComment = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the zeroPartId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZeroPartId() {
        return zeroPartId;
    }

    /**
     * Sets the value of the zeroPartId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZeroPartId(String value) {
        this.zeroPartId = value;
    }

}

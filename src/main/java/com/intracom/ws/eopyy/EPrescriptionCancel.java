
package com.intracom.ws.eopyy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ePrescriptionCancel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ePrescriptionCancel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EMessageNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prescriptNum" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="prescriptReferNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seqNum" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="supplBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ePrescriptionCancel", propOrder = {
    "eMessageNumber",
    "prescriptNum",
    "prescriptReferNumber",
    "seqNum",
    "supplBranchCode",
    "userId"
})
public class EPrescriptionCancel {

    @XmlElement(name = "EMessageNumber")
    protected String eMessageNumber;
    protected int prescriptNum;
    protected String prescriptReferNumber;
    protected int seqNum;
    protected String supplBranchCode;
    protected String userId;

    /**
     * Gets the value of the eMessageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMessageNumber() {
        return eMessageNumber;
    }

    /**
     * Sets the value of the eMessageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMessageNumber(String value) {
        this.eMessageNumber = value;
    }

    /**
     * Gets the value of the prescriptNum property.
     * 
     */
    public int getPrescriptNum() {
        return prescriptNum;
    }

    /**
     * Sets the value of the prescriptNum property.
     * 
     */
    public void setPrescriptNum(int value) {
        this.prescriptNum = value;
    }

    /**
     * Gets the value of the prescriptReferNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrescriptReferNumber() {
        return prescriptReferNumber;
    }

    /**
     * Sets the value of the prescriptReferNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrescriptReferNumber(String value) {
        this.prescriptReferNumber = value;
    }

    /**
     * Gets the value of the seqNum property.
     * 
     */
    public int getSeqNum() {
        return seqNum;
    }

    /**
     * Sets the value of the seqNum property.
     * 
     */
    public void setSeqNum(int value) {
        this.seqNum = value;
    }

    /**
     * Gets the value of the supplBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplBranchCode() {
        return supplBranchCode;
    }

    /**
     * Sets the value of the supplBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplBranchCode(String value) {
        this.supplBranchCode = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

}

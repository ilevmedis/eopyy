
package com.intracom.ws.eopyy;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.intracom.ws.eopyy package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CancelElectronicPrescriptionResponse_QNAME = new QName("http://eopyy.ws.intracom.com/", "cancelElectronicPrescriptionResponse");
    private final static QName _GetExamPrescription_QNAME = new QName("http://eopyy.ws.intracom.com/", "getExamPrescription");
    private final static QName _ExecuteElectronicPrescriptionResponse_QNAME = new QName("http://eopyy.ws.intracom.com/", "executeElectronicPrescriptionResponse");
    private final static QName _ExecuteElectronicPrescription_QNAME = new QName("http://eopyy.ws.intracom.com/", "executeElectronicPrescription");
    private final static QName _CancelElectronicPrescription_QNAME = new QName("http://eopyy.ws.intracom.com/", "cancelElectronicPrescription");
    private final static QName _GetExamPrescriptionResponse_QNAME = new QName("http://eopyy.ws.intracom.com/", "getExamPrescriptionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.intracom.ws.eopyy
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CancelElectronicPrescriptionResponse }
     * 
     */
    public CancelElectronicPrescriptionResponse createCancelElectronicPrescriptionResponse() {
        return new CancelElectronicPrescriptionResponse();
    }

    /**
     * Create an instance of {@link GetExamPrescription }
     * 
     */
    public GetExamPrescription createGetExamPrescription() {
        return new GetExamPrescription();
    }

    /**
     * Create an instance of {@link CancelElectronicPrescription }
     * 
     */
    public CancelElectronicPrescription createCancelElectronicPrescription() {
        return new CancelElectronicPrescription();
    }

    /**
     * Create an instance of {@link GetExamPrescriptionResponse }
     * 
     */
    public GetExamPrescriptionResponse createGetExamPrescriptionResponse() {
        return new GetExamPrescriptionResponse();
    }

    /**
     * Create an instance of {@link ExecuteElectronicPrescriptionResponse }
     * 
     */
    public ExecuteElectronicPrescriptionResponse createExecuteElectronicPrescriptionResponse() {
        return new ExecuteElectronicPrescriptionResponse();
    }

    /**
     * Create an instance of {@link ExecuteElectronicPrescription }
     * 
     */
    public ExecuteElectronicPrescription createExecuteElectronicPrescription() {
        return new ExecuteElectronicPrescription();
    }

    /**
     * Create an instance of {@link ResultBeanEPrescription }
     * 
     */
    public ResultBeanEPrescription createResultBeanEPrescription() {
        return new ResultBeanEPrescription();
    }

    /**
     * Create an instance of {@link PrescriptionDetailsList }
     * 
     */
    public PrescriptionDetailsList createPrescriptionDetailsList() {
        return new PrescriptionDetailsList();
    }

    /**
     * Create an instance of {@link SubmissionCancelOut }
     * 
     */
    public SubmissionCancelOut createSubmissionCancelOut() {
        return new SubmissionCancelOut();
    }

    /**
     * Create an instance of {@link ExaminationFirstSend }
     * 
     */
    public ExaminationFirstSend createExaminationFirstSend() {
        return new ExaminationFirstSend();
    }

    /**
     * Create an instance of {@link EprescriptionEntry }
     * 
     */
    public EprescriptionEntry createEprescriptionEntry() {
        return new EprescriptionEntry();
    }

    /**
     * Create an instance of {@link EPrescription }
     * 
     */
    public EPrescription createEPrescription() {
        return new EPrescription();
    }

    /**
     * Create an instance of {@link EPrescriptionException }
     * 
     */
    public EPrescriptionException createEPrescriptionException() {
        return new EPrescriptionException();
    }

    /**
     * Create an instance of {@link PrescriptionDetails }
     * 
     */
    public PrescriptionDetails createPrescriptionDetails() {
        return new PrescriptionDetails();
    }

    /**
     * Create an instance of {@link LoadFile }
     * 
     */
    public LoadFile createLoadFile() {
        return new LoadFile();
    }

    /**
     * Create an instance of {@link EPrescriptionCancel }
     * 
     */
    public EPrescriptionCancel createEPrescriptionCancel() {
        return new EPrescriptionCancel();
    }

    /**
     * Create an instance of {@link SubmissionOut }
     * 
     */
    public SubmissionOut createSubmissionOut() {
        return new SubmissionOut();
    }

    /**
     * Create an instance of {@link ExaminationFirst }
     * 
     */
    public ExaminationFirst createExaminationFirst() {
        return new ExaminationFirst();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelElectronicPrescriptionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eopyy.ws.intracom.com/", name = "cancelElectronicPrescriptionResponse")
    public JAXBElement<CancelElectronicPrescriptionResponse> createCancelElectronicPrescriptionResponse(CancelElectronicPrescriptionResponse value) {
        return new JAXBElement<CancelElectronicPrescriptionResponse>(_CancelElectronicPrescriptionResponse_QNAME, CancelElectronicPrescriptionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetExamPrescription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eopyy.ws.intracom.com/", name = "getExamPrescription")
    public JAXBElement<GetExamPrescription> createGetExamPrescription(GetExamPrescription value) {
        return new JAXBElement<GetExamPrescription>(_GetExamPrescription_QNAME, GetExamPrescription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteElectronicPrescriptionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eopyy.ws.intracom.com/", name = "executeElectronicPrescriptionResponse")
    public JAXBElement<ExecuteElectronicPrescriptionResponse> createExecuteElectronicPrescriptionResponse(ExecuteElectronicPrescriptionResponse value) {
        return new JAXBElement<ExecuteElectronicPrescriptionResponse>(_ExecuteElectronicPrescriptionResponse_QNAME, ExecuteElectronicPrescriptionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteElectronicPrescription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eopyy.ws.intracom.com/", name = "executeElectronicPrescription")
    public JAXBElement<ExecuteElectronicPrescription> createExecuteElectronicPrescription(ExecuteElectronicPrescription value) {
        return new JAXBElement<ExecuteElectronicPrescription>(_ExecuteElectronicPrescription_QNAME, ExecuteElectronicPrescription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelElectronicPrescription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eopyy.ws.intracom.com/", name = "cancelElectronicPrescription")
    public JAXBElement<CancelElectronicPrescription> createCancelElectronicPrescription(CancelElectronicPrescription value) {
        return new JAXBElement<CancelElectronicPrescription>(_CancelElectronicPrescription_QNAME, CancelElectronicPrescription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetExamPrescriptionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eopyy.ws.intracom.com/", name = "getExamPrescriptionResponse")
    public JAXBElement<GetExamPrescriptionResponse> createGetExamPrescriptionResponse(GetExamPrescriptionResponse value) {
        return new JAXBElement<GetExamPrescriptionResponse>(_GetExamPrescriptionResponse_QNAME, GetExamPrescriptionResponse.class, null, value);
    }

}

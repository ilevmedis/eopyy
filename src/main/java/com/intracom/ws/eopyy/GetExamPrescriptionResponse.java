
package com.intracom.ws.eopyy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getExamPrescriptionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getExamPrescriptionResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EPrescription" type="{http://eopyy.ws.intracom.com/}ePrescription" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getExamPrescriptionResponse", propOrder = {
    "ePrescription"
})
public class GetExamPrescriptionResponse {

    @XmlElement(name = "EPrescription")
    protected EPrescription ePrescription;

    /**
     * Gets the value of the ePrescription property.
     * 
     * @return
     *     possible object is
     *     {@link EPrescription }
     *     
     */
    public EPrescription getEPrescription() {
        return ePrescription;
    }

    /**
     * Sets the value of the ePrescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link EPrescription }
     *     
     */
    public void setEPrescription(EPrescription value) {
        this.ePrescription = value;
    }

}

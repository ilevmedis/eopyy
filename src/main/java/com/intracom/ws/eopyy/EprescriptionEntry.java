
package com.intracom.ws.eopyy;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eprescriptionEntry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="eprescriptionEntry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ama" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="approvalDateStr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="beneficiaryParapligicFlg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contractCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="EKAAExpiryDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EMessageNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ekaa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ekasBeneficiary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="euOrgid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="europeanFlg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="examinationFirstList" type="{http://eopyy.ws.intracom.com/}prescriptionDetailsList" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="examinationFirstSend" type="{http://eopyy.ws.intracom.com/}examinationFirstSend" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="examinedAmka" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="examinedFirstname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="examinedLastname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="examinedMobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="examinedPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="execDateStr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="execTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inspectorDocCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inspectorDocFirstname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inspectorDocLastname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="institutionCodeDescr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="intangible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="issueDateStr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="loadFile" type="{http://eopyy.ws.intracom.com/}loadFile" minOccurs="0"/>
 *         &lt;element name="militaryHealthCommitteFlg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noPrivateCompensation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="onlyPublicExecution" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prescrDocCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prescrDocFirstname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prescrDocLastname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="receiptDateStr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="receiptNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="supplBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="zeroPartId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eprescriptionEntry", propOrder = {
    "ama",
    "approvalDateStr",
    "beneficiaryParapligicFlg",
    "contractCode",
    "ekaaExpiryDate",
    "eMessageNumber",
    "ekaa",
    "ekasBeneficiary",
    "euOrgid",
    "europeanFlg",
    "examinationFirstList",
    "examinationFirstSend",
    "examinedAmka",
    "examinedFirstname",
    "examinedLastname",
    "examinedMobile",
    "examinedPhone",
    "execDateStr",
    "execTime",
    "inspectorDocCode",
    "inspectorDocFirstname",
    "inspectorDocLastname",
    "institutionCodeDescr",
    "insuredCountryCode",
    "intangible",
    "issueDateStr",
    "loadFile",
    "militaryHealthCommitteFlg",
    "noPrivateCompensation",
    "number",
    "onlyPublicExecution",
    "prescrDocCode",
    "prescrDocFirstname",
    "prescrDocLastname",
    "receiptDateStr",
    "receiptNumber",
    "supplBranchCode",
    "userId",
    "zeroPartId"
})
public class EprescriptionEntry {

    protected String ama;
    protected String approvalDateStr;
    protected String beneficiaryParapligicFlg;
    protected int contractCode;
    @XmlElement(name = "EKAAExpiryDate")
    protected String ekaaExpiryDate;
    @XmlElement(name = "EMessageNumber")
    protected String eMessageNumber;
    protected String ekaa;
    protected String ekasBeneficiary;
    protected String euOrgid;
    protected String europeanFlg;
    @XmlElement(nillable = true)
    protected List<PrescriptionDetailsList> examinationFirstList;
    @XmlElement(nillable = true)
    protected List<ExaminationFirstSend> examinationFirstSend;
    protected String examinedAmka;
    protected String examinedFirstname;
    protected String examinedLastname;
    protected String examinedMobile;
    protected String examinedPhone;
    protected String execDateStr;
    protected String execTime;
    protected String inspectorDocCode;
    protected String inspectorDocFirstname;
    protected String inspectorDocLastname;
    protected String institutionCodeDescr;
    protected String insuredCountryCode;
    protected String intangible;
    protected String issueDateStr;
    protected LoadFile loadFile;
    protected String militaryHealthCommitteFlg;
    protected String noPrivateCompensation;
    protected String number;
    protected String onlyPublicExecution;
    protected String prescrDocCode;
    protected String prescrDocFirstname;
    protected String prescrDocLastname;
    protected String receiptDateStr;
    protected String receiptNumber;
    protected String supplBranchCode;
    protected String userId;
    protected String zeroPartId;

    /**
     * Gets the value of the ama property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAma() {
        return ama;
    }

    /**
     * Sets the value of the ama property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAma(String value) {
        this.ama = value;
    }

    /**
     * Gets the value of the approvalDateStr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovalDateStr() {
        return approvalDateStr;
    }

    /**
     * Sets the value of the approvalDateStr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovalDateStr(String value) {
        this.approvalDateStr = value;
    }

    /**
     * Gets the value of the beneficiaryParapligicFlg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeneficiaryParapligicFlg() {
        return beneficiaryParapligicFlg;
    }

    /**
     * Sets the value of the beneficiaryParapligicFlg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeneficiaryParapligicFlg(String value) {
        this.beneficiaryParapligicFlg = value;
    }

    /**
     * Gets the value of the contractCode property.
     * 
     */
    public int getContractCode() {
        return contractCode;
    }

    /**
     * Sets the value of the contractCode property.
     * 
     */
    public void setContractCode(int value) {
        this.contractCode = value;
    }

    /**
     * Gets the value of the ekaaExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEKAAExpiryDate() {
        return ekaaExpiryDate;
    }

    /**
     * Sets the value of the ekaaExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEKAAExpiryDate(String value) {
        this.ekaaExpiryDate = value;
    }

    /**
     * Gets the value of the eMessageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMessageNumber() {
        return eMessageNumber;
    }

    /**
     * Sets the value of the eMessageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMessageNumber(String value) {
        this.eMessageNumber = value;
    }

    /**
     * Gets the value of the ekaa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEkaa() {
        return ekaa;
    }

    /**
     * Sets the value of the ekaa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEkaa(String value) {
        this.ekaa = value;
    }

    /**
     * Gets the value of the ekasBeneficiary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEkasBeneficiary() {
        return ekasBeneficiary;
    }

    /**
     * Sets the value of the ekasBeneficiary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEkasBeneficiary(String value) {
        this.ekasBeneficiary = value;
    }

    /**
     * Gets the value of the euOrgid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEuOrgid() {
        return euOrgid;
    }

    /**
     * Sets the value of the euOrgid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEuOrgid(String value) {
        this.euOrgid = value;
    }

    /**
     * Gets the value of the europeanFlg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEuropeanFlg() {
        return europeanFlg;
    }

    /**
     * Sets the value of the europeanFlg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEuropeanFlg(String value) {
        this.europeanFlg = value;
    }

    /**
     * Gets the value of the examinationFirstList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the examinationFirstList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExaminationFirstList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrescriptionDetailsList }
     * 
     * 
     */
    public List<PrescriptionDetailsList> getExaminationFirstList() {
        if (examinationFirstList == null) {
            examinationFirstList = new ArrayList<PrescriptionDetailsList>();
        }
        return this.examinationFirstList;
    }

    /**
     * Gets the value of the examinationFirstSend property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the examinationFirstSend property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExaminationFirstSend().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExaminationFirstSend }
     * 
     * 
     */
    public List<ExaminationFirstSend> getExaminationFirstSend() {
        if (examinationFirstSend == null) {
            examinationFirstSend = new ArrayList<ExaminationFirstSend>();
        }
        return this.examinationFirstSend;
    }

    /**
     * Gets the value of the examinedAmka property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExaminedAmka() {
        return examinedAmka;
    }

    /**
     * Sets the value of the examinedAmka property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExaminedAmka(String value) {
        this.examinedAmka = value;
    }

    /**
     * Gets the value of the examinedFirstname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExaminedFirstname() {
        return examinedFirstname;
    }

    /**
     * Sets the value of the examinedFirstname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExaminedFirstname(String value) {
        this.examinedFirstname = value;
    }

    /**
     * Gets the value of the examinedLastname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExaminedLastname() {
        return examinedLastname;
    }

    /**
     * Sets the value of the examinedLastname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExaminedLastname(String value) {
        this.examinedLastname = value;
    }

    /**
     * Gets the value of the examinedMobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExaminedMobile() {
        return examinedMobile;
    }

    /**
     * Sets the value of the examinedMobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExaminedMobile(String value) {
        this.examinedMobile = value;
    }

    /**
     * Gets the value of the examinedPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExaminedPhone() {
        return examinedPhone;
    }

    /**
     * Sets the value of the examinedPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExaminedPhone(String value) {
        this.examinedPhone = value;
    }

    /**
     * Gets the value of the execDateStr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExecDateStr() {
        return execDateStr;
    }

    /**
     * Sets the value of the execDateStr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExecDateStr(String value) {
        this.execDateStr = value;
    }

    /**
     * Gets the value of the execTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExecTime() {
        return execTime;
    }

    /**
     * Sets the value of the execTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExecTime(String value) {
        this.execTime = value;
    }

    /**
     * Gets the value of the inspectorDocCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInspectorDocCode() {
        return inspectorDocCode;
    }

    /**
     * Sets the value of the inspectorDocCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInspectorDocCode(String value) {
        this.inspectorDocCode = value;
    }

    /**
     * Gets the value of the inspectorDocFirstname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInspectorDocFirstname() {
        return inspectorDocFirstname;
    }

    /**
     * Sets the value of the inspectorDocFirstname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInspectorDocFirstname(String value) {
        this.inspectorDocFirstname = value;
    }

    /**
     * Gets the value of the inspectorDocLastname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInspectorDocLastname() {
        return inspectorDocLastname;
    }

    /**
     * Sets the value of the inspectorDocLastname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInspectorDocLastname(String value) {
        this.inspectorDocLastname = value;
    }

    /**
     * Gets the value of the institutionCodeDescr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstitutionCodeDescr() {
        return institutionCodeDescr;
    }

    /**
     * Sets the value of the institutionCodeDescr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstitutionCodeDescr(String value) {
        this.institutionCodeDescr = value;
    }

    /**
     * Gets the value of the insuredCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredCountryCode() {
        return insuredCountryCode;
    }

    /**
     * Sets the value of the insuredCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredCountryCode(String value) {
        this.insuredCountryCode = value;
    }

    /**
     * Gets the value of the intangible property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntangible() {
        return intangible;
    }

    /**
     * Sets the value of the intangible property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntangible(String value) {
        this.intangible = value;
    }

    /**
     * Gets the value of the issueDateStr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssueDateStr() {
        return issueDateStr;
    }

    /**
     * Sets the value of the issueDateStr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssueDateStr(String value) {
        this.issueDateStr = value;
    }

    /**
     * Gets the value of the loadFile property.
     * 
     * @return
     *     possible object is
     *     {@link LoadFile }
     *     
     */
    public LoadFile getLoadFile() {
        return loadFile;
    }

    /**
     * Sets the value of the loadFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoadFile }
     *     
     */
    public void setLoadFile(LoadFile value) {
        this.loadFile = value;
    }

    /**
     * Gets the value of the militaryHealthCommitteFlg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMilitaryHealthCommitteFlg() {
        return militaryHealthCommitteFlg;
    }

    /**
     * Sets the value of the militaryHealthCommitteFlg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMilitaryHealthCommitteFlg(String value) {
        this.militaryHealthCommitteFlg = value;
    }

    /**
     * Gets the value of the noPrivateCompensation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoPrivateCompensation() {
        return noPrivateCompensation;
    }

    /**
     * Sets the value of the noPrivateCompensation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoPrivateCompensation(String value) {
        this.noPrivateCompensation = value;
    }

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

    /**
     * Gets the value of the onlyPublicExecution property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnlyPublicExecution() {
        return onlyPublicExecution;
    }

    /**
     * Sets the value of the onlyPublicExecution property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnlyPublicExecution(String value) {
        this.onlyPublicExecution = value;
    }

    /**
     * Gets the value of the prescrDocCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrescrDocCode() {
        return prescrDocCode;
    }

    /**
     * Sets the value of the prescrDocCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrescrDocCode(String value) {
        this.prescrDocCode = value;
    }

    /**
     * Gets the value of the prescrDocFirstname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrescrDocFirstname() {
        return prescrDocFirstname;
    }

    /**
     * Sets the value of the prescrDocFirstname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrescrDocFirstname(String value) {
        this.prescrDocFirstname = value;
    }

    /**
     * Gets the value of the prescrDocLastname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrescrDocLastname() {
        return prescrDocLastname;
    }

    /**
     * Sets the value of the prescrDocLastname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrescrDocLastname(String value) {
        this.prescrDocLastname = value;
    }

    /**
     * Gets the value of the receiptDateStr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptDateStr() {
        return receiptDateStr;
    }

    /**
     * Sets the value of the receiptDateStr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptDateStr(String value) {
        this.receiptDateStr = value;
    }

    /**
     * Gets the value of the receiptNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * Sets the value of the receiptNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptNumber(String value) {
        this.receiptNumber = value;
    }

    /**
     * Gets the value of the supplBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplBranchCode() {
        return supplBranchCode;
    }

    /**
     * Sets the value of the supplBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplBranchCode(String value) {
        this.supplBranchCode = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the zeroPartId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZeroPartId() {
        return zeroPartId;
    }

    /**
     * Sets the value of the zeroPartId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZeroPartId(String value) {
        this.zeroPartId = value;
    }

}

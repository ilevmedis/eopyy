
package com.intracom.ws.eopyy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for examinationFirst complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="examinationFirst">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="icd10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="icd10Descr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sequenceNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="usedFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="usedFlg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eDapiCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "examinationFirst", propOrder = {
    "comments",
    "description",
    "icd10",
    "icd10Descr",
    "sequenceNumber",
    "usedFlag",
    "usedFlg",
    "eDapiCode"
})
public class ExaminationFirst {

    protected String comments;
    protected String description;
    protected String icd10;
    protected String icd10Descr;
    protected int sequenceNumber;
    protected String usedFlag;
    protected String usedFlg;
    protected String eDapiCode;

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the icd10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcd10() {
        return icd10;
    }

    /**
     * Sets the value of the icd10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcd10(String value) {
        this.icd10 = value;
    }

    /**
     * Gets the value of the icd10Descr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcd10Descr() {
        return icd10Descr;
    }

    /**
     * Sets the value of the icd10Descr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcd10Descr(String value) {
        this.icd10Descr = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     */
    public int getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     */
    public void setSequenceNumber(int value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the usedFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsedFlag() {
        return usedFlag;
    }

    /**
     * Sets the value of the usedFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsedFlag(String value) {
        this.usedFlag = value;
    }

    /**
     * Gets the value of the usedFlg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsedFlg() {
        return usedFlg;
    }

    /**
     * Sets the value of the usedFlg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsedFlg(String value) {
        this.usedFlg = value;
    }

    /**
     * Gets the value of the eDapiCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEDapiCode() {
        return eDapiCode;
    }

    /**
     * Sets the value of the eDapiCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEDapiCode(String value) {
        this.eDapiCode = value;
    }

}

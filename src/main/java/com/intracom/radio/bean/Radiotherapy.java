
package com.intracom.radio.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for radiotherapy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="radiotherapy">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cdaXml" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "radiotherapy", propOrder = {
    "cdaXml"
})
public class Radiotherapy {

    protected String cdaXml;

    /**
     * Gets the value of the cdaXml property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdaXml() {
        return cdaXml;
    }

    /**
     * Sets the value of the cdaXml property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdaXml(String value) {
        this.cdaXml = value;
    }

}

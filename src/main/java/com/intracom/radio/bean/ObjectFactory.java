
package com.intracom.radio.bean;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.intracom.radio.bean package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RadiotherapyResponse_QNAME = new QName("http://bean.intracom.com/", "radiotherapyResponse");
    private final static QName _Radiotherapy_QNAME = new QName("http://bean.intracom.com/", "radiotherapy");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.intracom.radio.bean
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ResultCDA }
     * 
     */
    public ResultCDA createResultCDA() {
        return new ResultCDA();
    }

    /**
     * Create an instance of {@link Radiotherapy }
     * 
     */
    public Radiotherapy createRadiotherapy() {
        return new Radiotherapy();
    }

    /**
     * Create an instance of {@link RadiotherapyResponse }
     * 
     */
    public RadiotherapyResponse createRadiotherapyResponse() {
        return new RadiotherapyResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RadiotherapyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "radiotherapyResponse")
    public JAXBElement<RadiotherapyResponse> createRadiotherapyResponse(RadiotherapyResponse value) {
        return new JAXBElement<RadiotherapyResponse>(_RadiotherapyResponse_QNAME, RadiotherapyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Radiotherapy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "radiotherapy")
    public JAXBElement<Radiotherapy> createRadiotherapy(Radiotherapy value) {
        return new JAXBElement<Radiotherapy>(_Radiotherapy_QNAME, Radiotherapy.class, null, value);
    }

}


package com.intracom.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for hl7ADTModelEU complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="hl7ADTModelEU">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hl7ADT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pdf" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="theFileName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hl7ADTModelEU", propOrder = {
    "hl7ADT",
    "pdf",
    "theFileName"
})
public class Hl7ADTModelEU {

    protected String hl7ADT;
    protected byte[] pdf;
    protected String theFileName;

    /**
     * Gets the value of the hl7ADT property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHl7ADT() {
        return hl7ADT;
    }

    /**
     * Sets the value of the hl7ADT property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHl7ADT(String value) {
        this.hl7ADT = value;
    }

    /**
     * Gets the value of the pdf property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getPdf() {
        return pdf;
    }

    /**
     * Sets the value of the pdf property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setPdf(byte[] value) {
        this.pdf = value;
    }

    /**
     * Gets the value of the theFileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheFileName() {
        return theFileName;
    }

    /**
     * Sets the value of the theFileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheFileName(String value) {
        this.theFileName = value;
    }

}


package com.intracom.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for hl7ADTModel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="hl7ADTModel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hl7ADT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hl7ADTModel", propOrder = {
    "hl7ADT"
})
public class Hl7ADTModel {

    protected String hl7ADT;

    /**
     * Gets the value of the hl7ADT property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHl7ADT() {
        return hl7ADT;
    }

    /**
     * Sets the value of the hl7ADT property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHl7ADT(String value) {
        this.hl7ADT = value;
    }

}

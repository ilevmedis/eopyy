
package com.intracom.bean;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.intracom.bean package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CancelDischargeHl7Response_QNAME = new QName("http://bean.intracom.com/", "cancelDischargeHl7Response");
    private final static QName _SaveDischargeHl7Response_QNAME = new QName("http://bean.intracom.com/", "saveDischargeHl7Response");
    private final static QName _CancelAdmissionHl7Response_QNAME = new QName("http://bean.intracom.com/", "cancelAdmissionHl7Response");
    private final static QName _CancelTransferHl7Response_QNAME = new QName("http://bean.intracom.com/", "cancelTransferHl7Response");
    private final static QName _SaveTransferHl7Response_QNAME = new QName("http://bean.intracom.com/", "saveTransferHl7Response");
    private final static QName _CancelDischargeHl7_QNAME = new QName("http://bean.intracom.com/", "cancelDischargeHl7");
    private final static QName _SaveAdmissionHl7_QNAME = new QName("http://bean.intracom.com/", "saveAdmissionHl7");
    private final static QName _CancelAdmissionHl7_QNAME = new QName("http://bean.intracom.com/", "cancelAdmissionHl7");
    private final static QName _SaveAdmissionHl7Response_QNAME = new QName("http://bean.intracom.com/", "saveAdmissionHl7Response");
    private final static QName _SaveTransferHl7_QNAME = new QName("http://bean.intracom.com/", "saveTransferHl7");
    private final static QName _CancelTransferHl7_QNAME = new QName("http://bean.intracom.com/", "cancelTransferHl7");
    private final static QName _SaveDischargeHl7_QNAME = new QName("http://bean.intracom.com/", "saveDischargeHl7");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.intracom.bean
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SaveDischargeHl7Response }
     * 
     */
    public SaveDischargeHl7Response createSaveDischargeHl7Response() {
        return new SaveDischargeHl7Response();
    }

    /**
     * Create an instance of {@link CancelDischargeHl7Response }
     * 
     */
    public CancelDischargeHl7Response createCancelDischargeHl7Response() {
        return new CancelDischargeHl7Response();
    }

    /**
     * Create an instance of {@link CancelAdmissionHl7Response }
     * 
     */
    public CancelAdmissionHl7Response createCancelAdmissionHl7Response() {
        return new CancelAdmissionHl7Response();
    }

    /**
     * Create an instance of {@link CancelAdmissionHl7 }
     * 
     */
    public CancelAdmissionHl7 createCancelAdmissionHl7() {
        return new CancelAdmissionHl7();
    }

    /**
     * Create an instance of {@link SaveAdmissionHl7Response }
     * 
     */
    public SaveAdmissionHl7Response createSaveAdmissionHl7Response() {
        return new SaveAdmissionHl7Response();
    }

    /**
     * Create an instance of {@link SaveTransferHl7 }
     * 
     */
    public SaveTransferHl7 createSaveTransferHl7() {
        return new SaveTransferHl7();
    }

    /**
     * Create an instance of {@link CancelDischargeHl7 }
     * 
     */
    public CancelDischargeHl7 createCancelDischargeHl7() {
        return new CancelDischargeHl7();
    }

    /**
     * Create an instance of {@link SaveAdmissionHl7 }
     * 
     */
    public SaveAdmissionHl7 createSaveAdmissionHl7() {
        return new SaveAdmissionHl7();
    }

    /**
     * Create an instance of {@link CancelTransferHl7Response }
     * 
     */
    public CancelTransferHl7Response createCancelTransferHl7Response() {
        return new CancelTransferHl7Response();
    }

    /**
     * Create an instance of {@link SaveTransferHl7Response }
     * 
     */
    public SaveTransferHl7Response createSaveTransferHl7Response() {
        return new SaveTransferHl7Response();
    }

    /**
     * Create an instance of {@link SaveDischargeHl7 }
     * 
     */
    public SaveDischargeHl7 createSaveDischargeHl7() {
        return new SaveDischargeHl7();
    }

    /**
     * Create an instance of {@link CancelTransferHl7 }
     * 
     */
    public CancelTransferHl7 createCancelTransferHl7() {
        return new CancelTransferHl7();
    }

    /**
     * Create an instance of {@link Hl7ADTModelEU }
     * 
     */
    public Hl7ADTModelEU createHl7ADTModelEU() {
        return new Hl7ADTModelEU();
    }

    /**
     * Create an instance of {@link Hl7ADTModel }
     * 
     */
    public Hl7ADTModel createHl7ADTModel() {
        return new Hl7ADTModel();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelDischargeHl7Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "cancelDischargeHl7Response")
    public JAXBElement<CancelDischargeHl7Response> createCancelDischargeHl7Response(CancelDischargeHl7Response value) {
        return new JAXBElement<CancelDischargeHl7Response>(_CancelDischargeHl7Response_QNAME, CancelDischargeHl7Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDischargeHl7Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "saveDischargeHl7Response")
    public JAXBElement<SaveDischargeHl7Response> createSaveDischargeHl7Response(SaveDischargeHl7Response value) {
        return new JAXBElement<SaveDischargeHl7Response>(_SaveDischargeHl7Response_QNAME, SaveDischargeHl7Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelAdmissionHl7Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "cancelAdmissionHl7Response")
    public JAXBElement<CancelAdmissionHl7Response> createCancelAdmissionHl7Response(CancelAdmissionHl7Response value) {
        return new JAXBElement<CancelAdmissionHl7Response>(_CancelAdmissionHl7Response_QNAME, CancelAdmissionHl7Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelTransferHl7Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "cancelTransferHl7Response")
    public JAXBElement<CancelTransferHl7Response> createCancelTransferHl7Response(CancelTransferHl7Response value) {
        return new JAXBElement<CancelTransferHl7Response>(_CancelTransferHl7Response_QNAME, CancelTransferHl7Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveTransferHl7Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "saveTransferHl7Response")
    public JAXBElement<SaveTransferHl7Response> createSaveTransferHl7Response(SaveTransferHl7Response value) {
        return new JAXBElement<SaveTransferHl7Response>(_SaveTransferHl7Response_QNAME, SaveTransferHl7Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelDischargeHl7 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "cancelDischargeHl7")
    public JAXBElement<CancelDischargeHl7> createCancelDischargeHl7(CancelDischargeHl7 value) {
        return new JAXBElement<CancelDischargeHl7>(_CancelDischargeHl7_QNAME, CancelDischargeHl7 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveAdmissionHl7 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "saveAdmissionHl7")
    public JAXBElement<SaveAdmissionHl7> createSaveAdmissionHl7(SaveAdmissionHl7 value) {
        return new JAXBElement<SaveAdmissionHl7>(_SaveAdmissionHl7_QNAME, SaveAdmissionHl7 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelAdmissionHl7 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "cancelAdmissionHl7")
    public JAXBElement<CancelAdmissionHl7> createCancelAdmissionHl7(CancelAdmissionHl7 value) {
        return new JAXBElement<CancelAdmissionHl7>(_CancelAdmissionHl7_QNAME, CancelAdmissionHl7 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveAdmissionHl7Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "saveAdmissionHl7Response")
    public JAXBElement<SaveAdmissionHl7Response> createSaveAdmissionHl7Response(SaveAdmissionHl7Response value) {
        return new JAXBElement<SaveAdmissionHl7Response>(_SaveAdmissionHl7Response_QNAME, SaveAdmissionHl7Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveTransferHl7 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "saveTransferHl7")
    public JAXBElement<SaveTransferHl7> createSaveTransferHl7(SaveTransferHl7 value) {
        return new JAXBElement<SaveTransferHl7>(_SaveTransferHl7_QNAME, SaveTransferHl7 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelTransferHl7 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "cancelTransferHl7")
    public JAXBElement<CancelTransferHl7> createCancelTransferHl7(CancelTransferHl7 value) {
        return new JAXBElement<CancelTransferHl7>(_CancelTransferHl7_QNAME, CancelTransferHl7 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDischargeHl7 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "saveDischargeHl7")
    public JAXBElement<SaveDischargeHl7> createSaveDischargeHl7(SaveDischargeHl7 value) {
        return new JAXBElement<SaveDischargeHl7>(_SaveDischargeHl7_QNAME, SaveDischargeHl7 .class, null, value);
    }

}

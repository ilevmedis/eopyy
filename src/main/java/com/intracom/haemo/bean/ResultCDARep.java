
package com.intracom.haemo.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for resultCDARep complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="resultCDARep">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="confirmationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="confirmationNo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="errors" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="recordSn" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theCurrentDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="theEncDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="theEncNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theRepNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resultCDARep", propOrder = {
    "confirmationDate",
    "confirmationNo",
    "errors",
    "recordSn",
    "status",
    "theCurrentDate",
    "theEncDate",
    "theEncNum",
    "theMessage",
    "theNumber",
    "theRepNum",
    "theUser"
})
public class ResultCDARep {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar confirmationDate;
    protected BigDecimal confirmationNo;
    @XmlElement(nillable = true)
    protected List<String> errors;
    protected BigDecimal recordSn;
    protected String status;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar theCurrentDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar theEncDate;
    protected String theEncNum;
    protected String theMessage;
    protected String theNumber;
    protected String theRepNum;
    protected String theUser;

    /**
     * Gets the value of the confirmationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getConfirmationDate() {
        return confirmationDate;
    }

    /**
     * Sets the value of the confirmationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setConfirmationDate(XMLGregorianCalendar value) {
        this.confirmationDate = value;
    }

    /**
     * Gets the value of the confirmationNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getConfirmationNo() {
        return confirmationNo;
    }

    /**
     * Sets the value of the confirmationNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setConfirmationNo(BigDecimal value) {
        this.confirmationNo = value;
    }

    /**
     * Gets the value of the errors property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errors property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrors().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getErrors() {
        if (errors == null) {
            errors = new ArrayList<String>();
        }
        return this.errors;
    }

    /**
     * Gets the value of the recordSn property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRecordSn() {
        return recordSn;
    }

    /**
     * Sets the value of the recordSn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRecordSn(BigDecimal value) {
        this.recordSn = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the theCurrentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTheCurrentDate() {
        return theCurrentDate;
    }

    /**
     * Sets the value of the theCurrentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTheCurrentDate(XMLGregorianCalendar value) {
        this.theCurrentDate = value;
    }

    /**
     * Gets the value of the theEncDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTheEncDate() {
        return theEncDate;
    }

    /**
     * Sets the value of the theEncDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTheEncDate(XMLGregorianCalendar value) {
        this.theEncDate = value;
    }

    /**
     * Gets the value of the theEncNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheEncNum() {
        return theEncNum;
    }

    /**
     * Sets the value of the theEncNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheEncNum(String value) {
        this.theEncNum = value;
    }

    /**
     * Gets the value of the theMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheMessage() {
        return theMessage;
    }

    /**
     * Sets the value of the theMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheMessage(String value) {
        this.theMessage = value;
    }

    /**
     * Gets the value of the theNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheNumber() {
        return theNumber;
    }

    /**
     * Sets the value of the theNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheNumber(String value) {
        this.theNumber = value;
    }

    /**
     * Gets the value of the theRepNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheRepNum() {
        return theRepNum;
    }

    /**
     * Sets the value of the theRepNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheRepNum(String value) {
        this.theRepNum = value;
    }

    /**
     * Gets the value of the theUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheUser() {
        return theUser;
    }

    /**
     * Sets the value of the theUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheUser(String value) {
        this.theUser = value;
    }

}

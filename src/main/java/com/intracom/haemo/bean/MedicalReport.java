
package com.intracom.haemo.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for medicalReport complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="medicalReport">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chk1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chk10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chk2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chk3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chk4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chk5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chk6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chk7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chk8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chk9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="countryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ehicEndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ehicInstitutionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ehicName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ehicNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ehicSurname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="grEuFatherName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="grEuSex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indirectAmka" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indirectEEAmka" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indirectEEFatherName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indirectEEName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indirectEESex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indirectEESurName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indirectName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indirectSex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indirectSurName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredFatherName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredSex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pedyNotes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theAMKA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theAMkADr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theAddressNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theAge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theAmaEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theAmkaGrEu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theBirthDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="theBodyArea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theChkNotes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theDateIssuePPA" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="theDateStartPPA" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="theDrName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theDrSurName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theFatherName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theHaemoNr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theHaemoType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theICDCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theICDCode2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theICDCode3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theICDCode4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theIndFather" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theInsOrgGrEu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theInsOrganisation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theInstRelated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theInstitution" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theInsuredId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theInsureredIdGrEu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theMobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theNameGrEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="thePhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="thePostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theRepNr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theReportDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="theReportNotes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theSurName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theSurnameGrEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theTown" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theTypeEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="theWeight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "medicalReport", propOrder = {
    "chk1",
    "chk10",
    "chk2",
    "chk3",
    "chk4",
    "chk5",
    "chk6",
    "chk7",
    "chk8",
    "chk9",
    "countryCode",
    "ehicEndDate",
    "ehicInstitutionCode",
    "ehicName",
    "ehicNumber",
    "ehicSurname",
    "grEuFatherName",
    "grEuSex",
    "indirectAmka",
    "indirectEEAmka",
    "indirectEEFatherName",
    "indirectEEName",
    "indirectEESex",
    "indirectEESurName",
    "indirectName",
    "indirectSex",
    "indirectSurName",
    "insuredFatherName",
    "insuredSex",
    "pedyNotes",
    "theAMKA",
    "theAMkADr",
    "theAddress",
    "theAddressNo",
    "theAge",
    "theAmaEU",
    "theAmkaGrEu",
    "theBirthDate",
    "theBodyArea",
    "theChkNotes",
    "theDateIssuePPA",
    "theDateStartPPA",
    "theDrName",
    "theDrSurName",
    "theEmail",
    "theFatherName",
    "theHaemoNr",
    "theHaemoType",
    "theICDCode",
    "theICDCode2",
    "theICDCode3",
    "theICDCode4",
    "theIndFather",
    "theInsOrgGrEu",
    "theInsOrganisation",
    "theInstRelated",
    "theInstitution",
    "theInsuredId",
    "theInsureredIdGrEu",
    "theMobile",
    "theName",
    "theNameGrEU",
    "thePhone",
    "thePostalCode",
    "theRepNr",
    "theReportDate",
    "theReportNotes",
    "theSurName",
    "theSurnameGrEU",
    "theTown",
    "theTypeEU",
    "theWeight"
})
public class MedicalReport {

    protected String chk1;
    protected String chk10;
    protected String chk2;
    protected String chk3;
    protected String chk4;
    protected String chk5;
    protected String chk6;
    protected String chk7;
    protected String chk8;
    protected String chk9;
    protected String countryCode;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ehicEndDate;
    protected String ehicInstitutionCode;
    protected String ehicName;
    protected String ehicNumber;
    protected String ehicSurname;
    protected String grEuFatherName;
    protected String grEuSex;
    protected String indirectAmka;
    protected String indirectEEAmka;
    protected String indirectEEFatherName;
    protected String indirectEEName;
    protected String indirectEESex;
    protected String indirectEESurName;
    protected String indirectName;
    protected String indirectSex;
    protected String indirectSurName;
    protected String insuredFatherName;
    protected String insuredSex;
    protected String pedyNotes;
    protected String theAMKA;
    protected String theAMkADr;
    protected String theAddress;
    protected String theAddressNo;
    protected String theAge;
    protected String theAmaEU;
    protected String theAmkaGrEu;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar theBirthDate;
    protected String theBodyArea;
    protected String theChkNotes;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar theDateIssuePPA;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar theDateStartPPA;
    protected String theDrName;
    protected String theDrSurName;
    protected String theEmail;
    protected String theFatherName;
    protected String theHaemoNr;
    protected String theHaemoType;
    protected String theICDCode;
    protected String theICDCode2;
    protected String theICDCode3;
    protected String theICDCode4;
    protected String theIndFather;
    protected String theInsOrgGrEu;
    protected String theInsOrganisation;
    protected String theInstRelated;
    protected String theInstitution;
    protected String theInsuredId;
    protected String theInsureredIdGrEu;
    protected String theMobile;
    protected String theName;
    protected String theNameGrEU;
    protected String thePhone;
    protected String thePostalCode;
    protected String theRepNr;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar theReportDate;
    protected String theReportNotes;
    protected String theSurName;
    protected String theSurnameGrEU;
    protected String theTown;
    protected String theTypeEU;
    protected String theWeight;

    /**
     * Gets the value of the chk1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChk1() {
        return chk1;
    }

    /**
     * Sets the value of the chk1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChk1(String value) {
        this.chk1 = value;
    }

    /**
     * Gets the value of the chk10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChk10() {
        return chk10;
    }

    /**
     * Sets the value of the chk10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChk10(String value) {
        this.chk10 = value;
    }

    /**
     * Gets the value of the chk2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChk2() {
        return chk2;
    }

    /**
     * Sets the value of the chk2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChk2(String value) {
        this.chk2 = value;
    }

    /**
     * Gets the value of the chk3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChk3() {
        return chk3;
    }

    /**
     * Sets the value of the chk3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChk3(String value) {
        this.chk3 = value;
    }

    /**
     * Gets the value of the chk4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChk4() {
        return chk4;
    }

    /**
     * Sets the value of the chk4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChk4(String value) {
        this.chk4 = value;
    }

    /**
     * Gets the value of the chk5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChk5() {
        return chk5;
    }

    /**
     * Sets the value of the chk5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChk5(String value) {
        this.chk5 = value;
    }

    /**
     * Gets the value of the chk6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChk6() {
        return chk6;
    }

    /**
     * Sets the value of the chk6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChk6(String value) {
        this.chk6 = value;
    }

    /**
     * Gets the value of the chk7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChk7() {
        return chk7;
    }

    /**
     * Sets the value of the chk7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChk7(String value) {
        this.chk7 = value;
    }

    /**
     * Gets the value of the chk8 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChk8() {
        return chk8;
    }

    /**
     * Sets the value of the chk8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChk8(String value) {
        this.chk8 = value;
    }

    /**
     * Gets the value of the chk9 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChk9() {
        return chk9;
    }

    /**
     * Sets the value of the chk9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChk9(String value) {
        this.chk9 = value;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the ehicEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEhicEndDate() {
        return ehicEndDate;
    }

    /**
     * Sets the value of the ehicEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEhicEndDate(XMLGregorianCalendar value) {
        this.ehicEndDate = value;
    }

    /**
     * Gets the value of the ehicInstitutionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEhicInstitutionCode() {
        return ehicInstitutionCode;
    }

    /**
     * Sets the value of the ehicInstitutionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEhicInstitutionCode(String value) {
        this.ehicInstitutionCode = value;
    }

    /**
     * Gets the value of the ehicName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEhicName() {
        return ehicName;
    }

    /**
     * Sets the value of the ehicName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEhicName(String value) {
        this.ehicName = value;
    }

    /**
     * Gets the value of the ehicNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEhicNumber() {
        return ehicNumber;
    }

    /**
     * Sets the value of the ehicNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEhicNumber(String value) {
        this.ehicNumber = value;
    }

    /**
     * Gets the value of the ehicSurname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEhicSurname() {
        return ehicSurname;
    }

    /**
     * Sets the value of the ehicSurname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEhicSurname(String value) {
        this.ehicSurname = value;
    }

    /**
     * Gets the value of the grEuFatherName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrEuFatherName() {
        return grEuFatherName;
    }

    /**
     * Sets the value of the grEuFatherName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrEuFatherName(String value) {
        this.grEuFatherName = value;
    }

    /**
     * Gets the value of the grEuSex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrEuSex() {
        return grEuSex;
    }

    /**
     * Sets the value of the grEuSex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrEuSex(String value) {
        this.grEuSex = value;
    }

    /**
     * Gets the value of the indirectAmka property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndirectAmka() {
        return indirectAmka;
    }

    /**
     * Sets the value of the indirectAmka property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndirectAmka(String value) {
        this.indirectAmka = value;
    }

    /**
     * Gets the value of the indirectEEAmka property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndirectEEAmka() {
        return indirectEEAmka;
    }

    /**
     * Sets the value of the indirectEEAmka property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndirectEEAmka(String value) {
        this.indirectEEAmka = value;
    }

    /**
     * Gets the value of the indirectEEFatherName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndirectEEFatherName() {
        return indirectEEFatherName;
    }

    /**
     * Sets the value of the indirectEEFatherName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndirectEEFatherName(String value) {
        this.indirectEEFatherName = value;
    }

    /**
     * Gets the value of the indirectEEName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndirectEEName() {
        return indirectEEName;
    }

    /**
     * Sets the value of the indirectEEName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndirectEEName(String value) {
        this.indirectEEName = value;
    }

    /**
     * Gets the value of the indirectEESex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndirectEESex() {
        return indirectEESex;
    }

    /**
     * Sets the value of the indirectEESex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndirectEESex(String value) {
        this.indirectEESex = value;
    }

    /**
     * Gets the value of the indirectEESurName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndirectEESurName() {
        return indirectEESurName;
    }

    /**
     * Sets the value of the indirectEESurName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndirectEESurName(String value) {
        this.indirectEESurName = value;
    }

    /**
     * Gets the value of the indirectName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndirectName() {
        return indirectName;
    }

    /**
     * Sets the value of the indirectName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndirectName(String value) {
        this.indirectName = value;
    }

    /**
     * Gets the value of the indirectSex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndirectSex() {
        return indirectSex;
    }

    /**
     * Sets the value of the indirectSex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndirectSex(String value) {
        this.indirectSex = value;
    }

    /**
     * Gets the value of the indirectSurName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndirectSurName() {
        return indirectSurName;
    }

    /**
     * Sets the value of the indirectSurName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndirectSurName(String value) {
        this.indirectSurName = value;
    }

    /**
     * Gets the value of the insuredFatherName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredFatherName() {
        return insuredFatherName;
    }

    /**
     * Sets the value of the insuredFatherName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredFatherName(String value) {
        this.insuredFatherName = value;
    }

    /**
     * Gets the value of the insuredSex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredSex() {
        return insuredSex;
    }

    /**
     * Sets the value of the insuredSex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredSex(String value) {
        this.insuredSex = value;
    }

    /**
     * Gets the value of the pedyNotes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPedyNotes() {
        return pedyNotes;
    }

    /**
     * Sets the value of the pedyNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPedyNotes(String value) {
        this.pedyNotes = value;
    }

    /**
     * Gets the value of the theAMKA property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheAMKA() {
        return theAMKA;
    }

    /**
     * Sets the value of the theAMKA property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheAMKA(String value) {
        this.theAMKA = value;
    }

    /**
     * Gets the value of the theAMkADr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheAMkADr() {
        return theAMkADr;
    }

    /**
     * Sets the value of the theAMkADr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheAMkADr(String value) {
        this.theAMkADr = value;
    }

    /**
     * Gets the value of the theAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheAddress() {
        return theAddress;
    }

    /**
     * Sets the value of the theAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheAddress(String value) {
        this.theAddress = value;
    }

    /**
     * Gets the value of the theAddressNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheAddressNo() {
        return theAddressNo;
    }

    /**
     * Sets the value of the theAddressNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheAddressNo(String value) {
        this.theAddressNo = value;
    }

    /**
     * Gets the value of the theAge property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheAge() {
        return theAge;
    }

    /**
     * Sets the value of the theAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheAge(String value) {
        this.theAge = value;
    }

    /**
     * Gets the value of the theAmaEU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheAmaEU() {
        return theAmaEU;
    }

    /**
     * Sets the value of the theAmaEU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheAmaEU(String value) {
        this.theAmaEU = value;
    }

    /**
     * Gets the value of the theAmkaGrEu property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheAmkaGrEu() {
        return theAmkaGrEu;
    }

    /**
     * Sets the value of the theAmkaGrEu property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheAmkaGrEu(String value) {
        this.theAmkaGrEu = value;
    }

    /**
     * Gets the value of the theBirthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTheBirthDate() {
        return theBirthDate;
    }

    /**
     * Sets the value of the theBirthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTheBirthDate(XMLGregorianCalendar value) {
        this.theBirthDate = value;
    }

    /**
     * Gets the value of the theBodyArea property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheBodyArea() {
        return theBodyArea;
    }

    /**
     * Sets the value of the theBodyArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheBodyArea(String value) {
        this.theBodyArea = value;
    }

    /**
     * Gets the value of the theChkNotes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheChkNotes() {
        return theChkNotes;
    }

    /**
     * Sets the value of the theChkNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheChkNotes(String value) {
        this.theChkNotes = value;
    }

    /**
     * Gets the value of the theDateIssuePPA property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTheDateIssuePPA() {
        return theDateIssuePPA;
    }

    /**
     * Sets the value of the theDateIssuePPA property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTheDateIssuePPA(XMLGregorianCalendar value) {
        this.theDateIssuePPA = value;
    }

    /**
     * Gets the value of the theDateStartPPA property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTheDateStartPPA() {
        return theDateStartPPA;
    }

    /**
     * Sets the value of the theDateStartPPA property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTheDateStartPPA(XMLGregorianCalendar value) {
        this.theDateStartPPA = value;
    }

    /**
     * Gets the value of the theDrName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheDrName() {
        return theDrName;
    }

    /**
     * Sets the value of the theDrName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheDrName(String value) {
        this.theDrName = value;
    }

    /**
     * Gets the value of the theDrSurName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheDrSurName() {
        return theDrSurName;
    }

    /**
     * Sets the value of the theDrSurName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheDrSurName(String value) {
        this.theDrSurName = value;
    }

    /**
     * Gets the value of the theEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheEmail() {
        return theEmail;
    }

    /**
     * Sets the value of the theEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheEmail(String value) {
        this.theEmail = value;
    }

    /**
     * Gets the value of the theFatherName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheFatherName() {
        return theFatherName;
    }

    /**
     * Sets the value of the theFatherName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheFatherName(String value) {
        this.theFatherName = value;
    }

    /**
     * Gets the value of the theHaemoNr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheHaemoNr() {
        return theHaemoNr;
    }

    /**
     * Sets the value of the theHaemoNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheHaemoNr(String value) {
        this.theHaemoNr = value;
    }

    /**
     * Gets the value of the theHaemoType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheHaemoType() {
        return theHaemoType;
    }

    /**
     * Sets the value of the theHaemoType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheHaemoType(String value) {
        this.theHaemoType = value;
    }

    /**
     * Gets the value of the theICDCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheICDCode() {
        return theICDCode;
    }

    /**
     * Sets the value of the theICDCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheICDCode(String value) {
        this.theICDCode = value;
    }

    /**
     * Gets the value of the theICDCode2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheICDCode2() {
        return theICDCode2;
    }

    /**
     * Sets the value of the theICDCode2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheICDCode2(String value) {
        this.theICDCode2 = value;
    }

    /**
     * Gets the value of the theICDCode3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheICDCode3() {
        return theICDCode3;
    }

    /**
     * Sets the value of the theICDCode3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheICDCode3(String value) {
        this.theICDCode3 = value;
    }

    /**
     * Gets the value of the theICDCode4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheICDCode4() {
        return theICDCode4;
    }

    /**
     * Sets the value of the theICDCode4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheICDCode4(String value) {
        this.theICDCode4 = value;
    }

    /**
     * Gets the value of the theIndFather property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheIndFather() {
        return theIndFather;
    }

    /**
     * Sets the value of the theIndFather property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheIndFather(String value) {
        this.theIndFather = value;
    }

    /**
     * Gets the value of the theInsOrgGrEu property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheInsOrgGrEu() {
        return theInsOrgGrEu;
    }

    /**
     * Sets the value of the theInsOrgGrEu property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheInsOrgGrEu(String value) {
        this.theInsOrgGrEu = value;
    }

    /**
     * Gets the value of the theInsOrganisation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheInsOrganisation() {
        return theInsOrganisation;
    }

    /**
     * Sets the value of the theInsOrganisation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheInsOrganisation(String value) {
        this.theInsOrganisation = value;
    }

    /**
     * Gets the value of the theInstRelated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheInstRelated() {
        return theInstRelated;
    }

    /**
     * Sets the value of the theInstRelated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheInstRelated(String value) {
        this.theInstRelated = value;
    }

    /**
     * Gets the value of the theInstitution property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheInstitution() {
        return theInstitution;
    }

    /**
     * Sets the value of the theInstitution property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheInstitution(String value) {
        this.theInstitution = value;
    }

    /**
     * Gets the value of the theInsuredId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheInsuredId() {
        return theInsuredId;
    }

    /**
     * Sets the value of the theInsuredId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheInsuredId(String value) {
        this.theInsuredId = value;
    }

    /**
     * Gets the value of the theInsureredIdGrEu property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheInsureredIdGrEu() {
        return theInsureredIdGrEu;
    }

    /**
     * Sets the value of the theInsureredIdGrEu property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheInsureredIdGrEu(String value) {
        this.theInsureredIdGrEu = value;
    }

    /**
     * Gets the value of the theMobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheMobile() {
        return theMobile;
    }

    /**
     * Sets the value of the theMobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheMobile(String value) {
        this.theMobile = value;
    }

    /**
     * Gets the value of the theName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheName() {
        return theName;
    }

    /**
     * Sets the value of the theName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheName(String value) {
        this.theName = value;
    }

    /**
     * Gets the value of the theNameGrEU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheNameGrEU() {
        return theNameGrEU;
    }

    /**
     * Sets the value of the theNameGrEU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheNameGrEU(String value) {
        this.theNameGrEU = value;
    }

    /**
     * Gets the value of the thePhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThePhone() {
        return thePhone;
    }

    /**
     * Sets the value of the thePhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThePhone(String value) {
        this.thePhone = value;
    }

    /**
     * Gets the value of the thePostalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThePostalCode() {
        return thePostalCode;
    }

    /**
     * Sets the value of the thePostalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThePostalCode(String value) {
        this.thePostalCode = value;
    }

    /**
     * Gets the value of the theRepNr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheRepNr() {
        return theRepNr;
    }

    /**
     * Sets the value of the theRepNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheRepNr(String value) {
        this.theRepNr = value;
    }

    /**
     * Gets the value of the theReportDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTheReportDate() {
        return theReportDate;
    }

    /**
     * Sets the value of the theReportDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTheReportDate(XMLGregorianCalendar value) {
        this.theReportDate = value;
    }

    /**
     * Gets the value of the theReportNotes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheReportNotes() {
        return theReportNotes;
    }

    /**
     * Sets the value of the theReportNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheReportNotes(String value) {
        this.theReportNotes = value;
    }

    /**
     * Gets the value of the theSurName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheSurName() {
        return theSurName;
    }

    /**
     * Sets the value of the theSurName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheSurName(String value) {
        this.theSurName = value;
    }

    /**
     * Gets the value of the theSurnameGrEU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheSurnameGrEU() {
        return theSurnameGrEU;
    }

    /**
     * Sets the value of the theSurnameGrEU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheSurnameGrEU(String value) {
        this.theSurnameGrEU = value;
    }

    /**
     * Gets the value of the theTown property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheTown() {
        return theTown;
    }

    /**
     * Sets the value of the theTown property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheTown(String value) {
        this.theTown = value;
    }

    /**
     * Gets the value of the theTypeEU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheTypeEU() {
        return theTypeEU;
    }

    /**
     * Sets the value of the theTypeEU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheTypeEU(String value) {
        this.theTypeEU = value;
    }

    /**
     * Gets the value of the theWeight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheWeight() {
        return theWeight;
    }

    /**
     * Sets the value of the theWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheWeight(String value) {
        this.theWeight = value;
    }

}

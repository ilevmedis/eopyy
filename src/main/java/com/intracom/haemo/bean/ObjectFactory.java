
package com.intracom.haemo.bean;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.intracom.haemo.bean package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RetrieveTheMedicalReportResponse_QNAME = new QName("http://bean.intracom.com/", "retrieveTheMedicalReportResponse");
    private final static QName _CancelReportResponse_QNAME = new QName("http://bean.intracom.com/", "cancelReportResponse");
    private final static QName _SubmmitTheCdaResponse_QNAME = new QName("http://bean.intracom.com/", "submmitTheCdaResponse");
    private final static QName _RetrieveTheMedicalReport_QNAME = new QName("http://bean.intracom.com/", "retrieveTheMedicalReport");
    private final static QName _CancelReport_QNAME = new QName("http://bean.intracom.com/", "cancelReport");
    private final static QName _SubmmitTheCda_QNAME = new QName("http://bean.intracom.com/", "submmitTheCda");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.intracom.haemo.bean
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CancelReportResponse }
     * 
     */
    public CancelReportResponse createCancelReportResponse() {
        return new CancelReportResponse();
    }

    /**
     * Create an instance of {@link SubmmitTheCda }
     * 
     */
    public SubmmitTheCda createSubmmitTheCda() {
        return new SubmmitTheCda();
    }

    /**
     * Create an instance of {@link MedicalReport }
     * 
     */
    public MedicalReport createMedicalReport() {
        return new MedicalReport();
    }

    /**
     * Create an instance of {@link RetrieveTheMedicalReportResponse }
     * 
     */
    public RetrieveTheMedicalReportResponse createRetrieveTheMedicalReportResponse() {
        return new RetrieveTheMedicalReportResponse();
    }

    /**
     * Create an instance of {@link ResultCDARep }
     * 
     */
    public ResultCDARep createResultCDARep() {
        return new ResultCDARep();
    }

    /**
     * Create an instance of {@link SubmmitTheCdaResponse }
     * 
     */
    public SubmmitTheCdaResponse createSubmmitTheCdaResponse() {
        return new SubmmitTheCdaResponse();
    }

    /**
     * Create an instance of {@link RetrieveTheMedicalReport }
     * 
     */
    public RetrieveTheMedicalReport createRetrieveTheMedicalReport() {
        return new RetrieveTheMedicalReport();
    }

    /**
     * Create an instance of {@link CancelReport }
     * 
     */
    public CancelReport createCancelReport() {
        return new CancelReport();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveTheMedicalReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "retrieveTheMedicalReportResponse")
    public JAXBElement<RetrieveTheMedicalReportResponse> createRetrieveTheMedicalReportResponse(RetrieveTheMedicalReportResponse value) {
        return new JAXBElement<RetrieveTheMedicalReportResponse>(_RetrieveTheMedicalReportResponse_QNAME, RetrieveTheMedicalReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "cancelReportResponse")
    public JAXBElement<CancelReportResponse> createCancelReportResponse(CancelReportResponse value) {
        return new JAXBElement<CancelReportResponse>(_CancelReportResponse_QNAME, CancelReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmmitTheCdaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "submmitTheCdaResponse")
    public JAXBElement<SubmmitTheCdaResponse> createSubmmitTheCdaResponse(SubmmitTheCdaResponse value) {
        return new JAXBElement<SubmmitTheCdaResponse>(_SubmmitTheCdaResponse_QNAME, SubmmitTheCdaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveTheMedicalReport }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "retrieveTheMedicalReport")
    public JAXBElement<RetrieveTheMedicalReport> createRetrieveTheMedicalReport(RetrieveTheMedicalReport value) {
        return new JAXBElement<RetrieveTheMedicalReport>(_RetrieveTheMedicalReport_QNAME, RetrieveTheMedicalReport.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelReport }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "cancelReport")
    public JAXBElement<CancelReport> createCancelReport(CancelReport value) {
        return new JAXBElement<CancelReport>(_CancelReport_QNAME, CancelReport.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmmitTheCda }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bean.intracom.com/", name = "submmitTheCda")
    public JAXBElement<SubmmitTheCda> createSubmmitTheCda(SubmmitTheCda value) {
        return new JAXBElement<SubmmitTheCda>(_SubmmitTheCda_QNAME, SubmmitTheCda.class, null, value);
    }

}
